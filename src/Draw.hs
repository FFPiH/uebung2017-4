{-# Language DeriveFunctor #-}
{-# Language DeriveGeneric #-}
{-# Language TemplateHaskell #-}

module Draw
        ( Options(..)
        , VisObject(..)
        , Color(..)
        , Drawable(..)
        , display
        , display'
        , simulate
        , simulate'
        , play
        , play'
        , Event(..)
        ) where

import GHC.Generics ( Generic )
import Data.Maybe (fromMaybe, isNothing)
import Control.Monad (unless)
import UI.NCurses hiding (Color)
import System.CPUTime
import Control.Monad.IfElse
import Control.Monad.IO.Class
import Control.Lens
import Linear.V2

data Color = White | Black | Red | Green deriving (Eq,Show)

data Options =
  Options
  { _optBackgroundColor :: Maybe Color             -- ^ optional background color
  , _optWindowSize      :: Maybe (Integer,Integer) -- ^ optional (row,col) window size
  , _optWindowPosition  :: Maybe (Integer,Integer) -- ^ optional (row,col) window origin
  } deriving Show

makeLenses (''Options)

data VisObject a = VisObjects [VisObject a]
                 | Trans (V2 Integer) (VisObject a)
                 | LineV (V2 Integer)  a  Color
                 | LineH (V2 Integer)  a  Color
                 | Char  (V2 Integer)  a  Color
                 | Text  (V2 Integer) [a] Color
                 deriving (Generic, Functor)

class Drawable a where
        c :: a -> Char

instance Drawable Char where
        c = id

type ColorCache = (ColorID, ColorID, ColorID, ColorID)

initColors :: Curses ColorCache
initColors = do
        cWhite <- newColorID ColorWhite ColorWhite 10
        cBlack <- newColorID ColorBlack ColorWhite 11
        cRed   <- newColorID ColorRed   ColorWhite 12
        cGreen <- newColorID ColorGreen ColorWhite 13
        return ( cWhite, cBlack, cRed, cGreen)

white = _1
black = _2
red   = _3
green = _4

initWindow :: Options -> Curses (ColorCache, Window)
initWindow options = do
        win <- newWindow
                  (fromMaybe 60 $ options^?optWindowSize._Just._1)
                  (fromMaybe 80 $ options^?optWindowSize._Just._2)
                  (fromMaybe 0  $ options^?optWindowPosition._Just._2)
                  (fromMaybe 0  $ options^?optWindowPosition._Just._1)
        colors  <- initColors
        bgRed   <- newColorID ColorWhite ColorRed   14
        bgGreen <- newColorID ColorWhite ColorGreen 15
        let bgCol Black = colors^.white
            bgCol White = colors^.black
            bgCol Red   = bgRed
            bgCol Green = bgGreen
        updateWindow win $ setColor $ bgCol
                                (fromMaybe Black $ options^.optBackgroundColor)
        return (colors, win)

display :: Drawable a => Options -> VisObject a -> Curses ()
display options toDraw = do
        (colors, win) <- initWindow options
        updateWindow win $ clear >> rawDraw colors toDraw
        render
        waitKeyEvent win

display' options toDraw = runCurses $ display options toDraw 

waitKeyEvent :: Window -> Curses ()
waitKeyEvent win = do
        let isKeyEvent (Just (EventCharacter _))  = True
            isKeyEvent (Just (EventSpecialKey _)) = True
            isKeyEvent _                   = False
        event <- getEvent win Nothing
        unless (isKeyEvent event) $ waitKeyEvent win

waitQKey :: Window -> Integer -> Curses Bool
waitQKey win timeout = do
        start <- liftIO getCPUTime
        let isQKey (Just (EventCharacter 'q')) = True
            isQKey _                           = False
        event <- getEvent win $ Just timeout
        if isNothing event
           then return False
           else if isQKey event
           then return True
           else do
                   now <- liftIO getCPUTime
                   waitQKey win $ timeout - round
                     ((fromIntegral (now - start)::Double) / (10^9::Double))

simulate' options sampleRate model drawFunction simulateFunction = runCurses $ simulate options sampleRate model drawFunction simulateFunction
simulate :: (Drawable a) => Options -> Integer -> world -> (world -> VisObject a) -> (Double -> world -> world) -> Curses ()
simulate options sampleRate model drawFunction simulateFunction = do
        (colors, win) <- initWindow options
        let simulateLoop model = do
              start <- liftIO getCPUTime
              updateWindow win $ clear >> rawDraw colors (drawFunction model)
              render
              isQKey <- waitQKey win sampleRate
              unless isQKey $ do
                      now <- liftIO getCPUTime
                      simulateLoop $ simulateFunction ((fromIntegral (now - start)::Double) / 10^9) model

        simulateLoop model

play' options sampleRate model drawFunction simulateFunction eventHamdler =
        runCurses $ play options sampleRate model drawFunction simulateFunction eventHamdler
play :: (Drawable a)
     => Options
     -> Integer
     -> world
     -> (world -> VisObject a)
     -> (Double -> world -> world)
     -> (Event -> world -> world)
     -> Curses ()
play options sampleRate model drawFunction simulateFunction eventHamdler = do
        (colors, win) <- initWindow options
        let playLoop model = do
              start <- liftIO getCPUTime
              updateWindow win $ clear >> rawDraw colors (drawFunction model)
              render
              event <- getEvent win $ Just sampleRate
              now <- liftIO getCPUTime
              let model' = simulateFunction ((fromIntegral (now - start)::Double) / 10^9) model
                  model'' = case event of
                              (Just event') -> eventHamdler event' model'
                              _             -> model'
              playLoop model''
        playLoop model


rawDraw :: Drawable a => ColorCache -> VisObject a -> Update ()
rawDraw colors (VisObjects a) = sequence_ $ rawDraw colors <$> a
rawDraw colors (Trans t d) = rawDraw colors $ translate t d
rawDraw colors (Text pos t col) = do
        moveCursor (pos^._x) (pos^._y)
        sequence_ $ drawC <$> t
        where drawC a = drawGlyph $ Glyph (a^.to c) [col2Attr colors col]
rawDraw colors (Char pos a color) = do
        moveCursor (pos^._x) (pos^._y)
        drawGlyph $ Glyph (a^.to c) [color^.to attrColor]
                where attrColor = col2Attr colors
rawDraw colors (LineH pos a color) = do
        moveCursor (pos^._x) 0
        drawLineH (Just $ Glyph (a^.to c) [col2Attr colors color]) 20
rawDraw colors (LineV pos a color) = do
        moveCursor 0 (pos^._y)
        drawLineV (Just $ Glyph (a^.to c) [col2Attr colors color]) 20

col2Attr :: ColorCache -> Color -> Attribute
col2Attr colors White = AttributeColor $ colors^.white
col2Attr colors Black = AttributeColor $ colors^.black
col2Attr colors Green = AttributeColor $ colors^.green
col2Attr colors Red   = AttributeColor $ colors^.red

translate :: V2 Integer -> VisObject a -> VisObject a
translate t (VisObjects a) = VisObjects $ translate t <$> a
translate t (Trans t' d) = translate (t+t') d
translate t (Text pos a c) = Text (pos+t) a c
translate t (LineV pos a c) = LineV (pos+t & _x .~ 0) a c
translate t (LineH pos a c) = LineH (pos+t & _y .~ 0) a c
translate t (Char pos a c) = Char (pos+t) a c

