{-# Language TemplateHaskell #-}
module GameConfig
  ( GameConfig(..)
  , defaultConfig
  , getAction
  , worldSize
  , ncursesOpts
  , keyMap
  ) where

import Draw
import Control.Lens
import GameTypes
import Linear

-- | Holds the basic configurations of the game.
data GameConfig = GameConfig
  { _worldSize   :: (Integer, Integer)
  , _ncursesOpts :: Options
  , _keyMap      :: Char -> Maybe Action
  }

-- | Specifies a game configuration for a given size, background color and key mapping.   
makeConfig :: (Integer, Integer) -> Maybe Color -> (Char -> Maybe Action) -> GameConfig
makeConfig size optBackgroundColor keyMapping = GameConfig size opts keyMapping
  where opts = Options optBackgroundColor (Just $ size & _1 +~ 5 & _2 +~ 5) Nothing

-- | Default game configuration.
defaultConfig :: GameConfig
defaultConfig = makeConfig (20, 32) (Just Red) getAction

-- | Default key mapping.
getAction :: Char -> Maybe Action
getAction 'w' = Just $ Move North
getAction 'a' = Just $ Move West
getAction 's' = Just $ Move South
getAction 'd' = Just $ Move East
getAction ' ' = Just Attack
getAction 'p' = Just Pause
getAction 't' = Just SetTrap
getAction _   = Nothing


-- * Lenses for the GameConfig type

$(makeLenses ''GameConfig)

