module GameDraw 
        ( toVisObject
        , renderWorld
        ) where

import Draw
import GameTypes
import Linear
import Control.Lens
import qualified Data.Map as M

toVisObject :: (V2 Integer, Entity) -> VisObject Char
toVisObject (p, Hero s)  = 
  VisObjects [ Text (V2 1 0) ("Life: " ++ s^.life.to show ++ " ") Green
             , case () of
                    _ | s^.orient == North -> Char p 'N' Green
                      | s^.orient == South -> Char p 'S' Green
                      | s^.orient == East  -> Char p 'E' Green
                      | s^.orient == West  -> Char p 'W' Green
             ]
toVisObject (p, Boss s)  = 
  VisObjects [ Text (V2 0 0) ("Bosslife: " ++ s^.life.to show ++ " ") Red
             , Char p 'M' Black
             ]
toVisObject (p, Floor)                = Char p '-' White
toVisObject (p, Wall)                 = Char p '#' Black
toVisObject (p, Bomb _)               = Char p '💣' Black
toVisObject (p, Fire s) | s^.life < 2 = Char p '🔥' Red
                        | otherwise   = Char p '🔥' Red

renderWorld :: World -> VisObject Char
renderWorld w = 
 let map = toVisObject . (_1._x +~ (2::Integer)) <$> (w^.entities.to M.toList)
     wonMsg     = Text (V2 1 12) " WON! " Green
     lostMsg    = Text (V2 1 12) " Lost " Red
     heroIsDead = w^.entities.at (w^.heroPos)^?_Just._Hero.life.to (<=0) & and
 in case w^.mode of
         Ended | heroIsDead -> VisObjects $ lostMsg : map
               | otherwise  -> VisObjects $ wonMsg  : map
         _                  -> VisObjects map
