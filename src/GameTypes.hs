{-# Language TemplateHaskell #-}
module GameTypes where

import Control.Lens
import Linear
import qualified Data.Map as M

{- * Types-}

-- | World represents the current game state with a game mode, 
--   a turn counter, a map of entitites and the hero's position.
data World
  = World
  { _mode     :: Mode
  , _turn     :: Integer
  , _entities :: Dungeon
  , _heroPos  :: Position
  } deriving (Show,Eq)

-- | Mode can be one of three things: 
--
--   - Active – the game is currently running; player can control hero 
--   - Ended – the game has ended; hero or boss is dead 
--   - Paused – the game is paused; player can only unpause or restart the game 
data Mode 
  = Active
  | Ended
  | Paused
   deriving (Show,Eq)

-- | A lazy Data.Map which associates all the entities in the 
--   dungeon with its coordinates (Position)
type Dungeon = M.Map Position Entity

-- | Position of an entity in the world
type Position = V2 Integer

-- | Entity can be one of six things: 
-- 
--   - Hero – under the control of the player
--   - Boss – controlled by the computer; fights the hero with bombs
--   - Floor – empty tile in the dungeon
--   - Wall – cannot be passed
--   - Bomb – thrown by enemies; eventually exploding
--   - Fire – burning tile emerging from explosions
data Entity
  = Hero  { _stats :: Stats }
  | Boss  { _stats :: Stats }
  | Bomb  { _stats :: Stats }
  | Fire  { _stats :: Stats }
  | Trap
  | Floor
  | Wall
  deriving (Show, Eq)

-- | Stats represents two things: 
-- 
--   - Orientation – the direction, in which an entity is oriented
--   - Life – the life energy or (!) life time of an entity
data Stats
  = Stats
  { _orient  :: Direction
  , _life    :: Integer
  } deriving (Show,Eq)

-- | Direction vector
type DirVector = V2 Integer

-- | Direction of an entity in the world
data Direction 
  = North
  | South
  | West
  | East
  deriving (Show, Eq)

-- | Converts to a vector (V2 Integer)
toVector :: Direction -> DirVector
toVector North = V2 (-1)  0
toVector South = V2   1   0
toVector West  = V2   0 (-1)
toVector East  = V2   0   1

-- | Gets a Direction from a vector (V2 Integer) 
fromVector :: DirVector -> Direction
fromVector vec =
  case abs <$> vec of
       v | v^._x >  v^._y && vec^._x >  0 -> South
         | v^._x >  v^._y && vec^._x <= 0 -> North
         | v^._x <= v^._y && vec^._y >  0 -> East
         | otherwise                      -> West

-- | Action can be one of three things
-- 
--   - Move – the player moves the hero in a specified direction
--   - Attack – the player attacks the location in front of the hero
--   - Pause – the player pauses the game
--   - SetTrap – the player sets a trap in front of the hero
data Action 
  = Move  { _movDir :: Direction }
  | Attack
  | Pause
  | SetTrap
   deriving (Show,Eq)

{- * Lenses for the World type -}
$(makeLenses ''World)

{- * Prisms for the Entity type -}
$(makePrisms ''Entity)

{- * Traversals for the Entity type -}
$(makeLenses ''Entity)

{- * Lenses for the Stats type -}
$(makeLenses ''Stats)

{- * Prisms for the Action type -}
$(makePrisms ''Action)

{- * Traversals for the Action type -}
$(makeLenses ''Action)


